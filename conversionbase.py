def conversion_base(n, b):
    L = []
    while n != 0:
        reste = n % b
        L.insert(0, reste)
        n = n // b
    for i in range(len(L)):
        if L[i] >= 10:
            L[i] = chr(L[i] - 10 + ord('A'))
        else:
            L[i] = str(L[i])
    resultat = ''.join(L)
    return resultat
def binaire_vers_hexa(binaire):
   
    # Créer un dictionnaire pour convertir les valeurs binaires en hexadécimal
    dico = {'0000': '0', '0001': '1', '0010': '2', '0011': '3',
            '0100': '4', '0101': '5', '0110': '6', '0111': '7',
            '1000': '8', '1001': '9', '1010': 'A', '1011': 'B',
            '1100': 'C', '1101': 'D', '1110': 'E', '1111': 'F'}
    
    # Ajouter des zéros à gauche pour obtenir un nombre de bits multiple de 4
    while len(binaire) % 4 != 0:
        binaire = '0' + binaire
    
    # Diviser le nombre binaire en paquets de 4 bits, en commençant par le bit de poids fort
    paquets = [binaire[i:i+4] for i in range(0, len(binaire), 4)]
    
    # Convertir chaque paquet de 4 bits en un chiffre hexadécimal, en utilisant le dictionnaire
    resultat = ''.join([dico[paquet] for paquet in paquets])
    
    # Retourner le nombre hexadécimal obtenu
    return resultat

def base_vers_decimal(n, b):
    """
    Convertit un nombre dans une base b vers la base décimale (base 10).

    Arguments :
    n -- un entier ou une chaîne de caractères représentant le nombre dans la base b
    b -- un entier représentant la base (b >= 2)

    Retour :
    un entier représentant le nombre dans la base décimale
    """
    # Convertir n en une chaîne de caractères si nécessaire
    if not isinstance(n, str):
        n = str(n)
    
    # Initialiser les variables
    resultat = 0
    poids = 1
    
    # Parcourir les caractères de la chaîne de caractères de droite à gauche
    for i in range(len(n)-1, -1, -1):
        # Convertir le caractère en un nombre entier dans la base b
        x = int(n[i], b)
        # Ajouter x multiplié par le poids au résultat
        resultat += x * poids
        # Multiplier le poids par b pour passer au caractère suivant
        poids *= b
    
    # Retourner le résultat
    return resultat
def base_vers_base(nombre, base, nouvelle_base):
    # Vérifier que la chaîne de caractères ne contient que des chiffres compris entre 0 et base-1 inclus
    for chiffre in str(nombre):
        if chiffre.isdigit():
            if int(chiffre) >= base:
                raise ValueError("Le nombre n'est pas valide pour cette base")
        else:
            if ord(chiffre.upper()) - ord('A') + 10 >= base:
                raise ValueError("Le nombre n'est pas valide pour cette base")
    
    # Convertir le nombre en base 10
    nombre_decimal = 0
    for i, chiffre in enumerate(reversed(str(nombre))):
        if chiffre.isdigit():
            nombre_decimal += int(chiffre) * pow(base, i)
        else:
            nombre_decimal += (ord(chiffre.upper()) - ord('A') + 10) * pow(base, i)
    
    # Convertir le nombre en la nouvelle base
    chiffres_nouvelle_base = []
    while nombre_decimal > 0:
        reste = nombre_decimal % nouvelle_base
        if reste < 10:
            chiffres_nouvelle_base.append(str(reste))
        else:
            chiffres_nouvelle_base.append(chr(reste - 10 + ord('A')))
        nombre_decimal //= nouvelle_base
    
    chiffres_nouvelle_base.reverse()
    resultat = ''.join(chiffres_nouvelle_base)
    
    return resultat
def convert_to_binary(num, precision):
    # Déterminer le signe du nombre et le stocker
    if num < 0:
        sign = "-"
        num = abs(num)
    else:
        sign = ""
    
    # Séparer la partie entière et la partie décimale
    integer_part = int(num)
    decimal_part = num - integer_part
    
    # Convertir la partie entière en binaire
    binary_integer = bin(integer_part)[2:]
    
    # Convertir la partie décimale en binaire
    binary_decimal = ""
    for i in range(precision):
        decimal_part *= 2
        if decimal_part >= 1:
            binary_decimal += "1"
            decimal_part -= 1
        else:
            binary_decimal += "0"
    
    # Combiner les parties entière et décimale
    binary_num = binary_integer + "." + binary_decimal
    
    # Ajouter le signe si nécessaire
    if sign:
        binary_num = sign + binary_num
    
    return binary_num

# print(conversion_base(11235,59))
# print(binaire_vers_hexa('1010001'))
# print(base_vers_decimal('1010001',2))
# while(True):
#     val=float(input('nombre: '))
#     print(convert_to_binary(val,5))



