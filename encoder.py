import heapq
import codecs
from utilities import *
from conversionbase import base_vers_decimal
class Node:
    def __init__(self,proba,index,symbol=None,left=None,right=None):
        self.proba = proba
        self.symbol = symbol
        self.left = left
        self.right = right
        self.index = index
    def __lt__(self, other):
        return self.proba < other.proba

    def __eq__(self, other):
        return self.proba == other.proba

class Encoder:
    @staticmethod
    def huffman(S):
        heap = []
        i=0
        for symbol, proba in S.items():
            heapq.heappush(heap, Node(proba,i, symbol))
            i+=1
        while len(heap) > 1:
            el1=heapq.heappop(heap)
            el2=heapq.heappop(heap)
            Source1,Source2=el1,el2
            if(el1.index>el2.index):
                Source1=el2
                Source2=el1
            merged = Node(Source1.proba+Source2.proba,int(Source2.index)-1,symbol=Source1.symbol+Source2.symbol,left=Source1,right=Source2)
            heapq.heappush(heap,merged)
        root = heapq.heappop(heap)
        codes = {}
        def traverse(node, code,S):
            try:
                if S[node.symbol]!=None:
                    codes[node.symbol] = code
            except KeyError:
                traverse(node.left, code + "1",S)
                traverse(node.right, code + "0",S)
        traverse(root, "",S)
        return codes

    def __init__(self,S=None,message=None):
        if message is not None:
            self.stat=self.manytimes(message)
            self.S = self.toProb(self.stat,len(message))
            self.dictionary=self.huffman(self.S)
        self.message=message
    

    def encode(self):
        dic=self.dictionary
        retour=self.message
        vaovao=''
        for i in retour:
            vaovao+=dic[i]
        return vaovao


    def decode(self,message,dictionary=None):
        # dic=(dictionary is not None)?dictionary:self.dictionary
        if dictionary is not None:
            dic=self.huffman(dictionary)
        else:
            dic=self.dictionary
        retour=''
        temp=message
        i=0
        while i<len(temp):
            for symbol, code in dic.items():
                if temp[:i+1]==code:
                    retour+=symbol
                    temp=temp[len(code):]
                    i=-1
                    break
            i+=1
                
        return retour
    @staticmethod
    def manytimes(text):
        # Initialize an empty dictionary to store the counts of each character
        counts = {}
        
        # Loop over each character in the text and count its occurrences
        for char in text:
            if char in counts:
                counts[char] += 1
            else:
                counts[char] = 1
        
        
        
        # Convert the probabilities dictionary to a tuple and return it
        return counts
    @staticmethod
    def toProb(counts,alava):
        probabilities = {char: count/alava for char, count in counts.items()}
        return probabilities

    @staticmethod
    def compress(filename,o):
        with open(filename,'r') as input,open(o,'wb') as output,open(o+'.map','wb') as dicomap:
            text=input.read()
            a=Encoder(message=text)
            alava=len(text)
            #head
            header=f"{alava}\n"
            #the encoded message
            encode_message=a.encode()
            # print(f"message: {encode_message}\n")
            dictionary=a.dictionary
            
            for char,isa in a.stat.items():
                header += f"{ord(char)}:{bin(isa)}:{dictionary[char]}\n"
            
            #write mapper
            dicomap.write(header.encode())

            lastremain=len(encode_message)%8

            liste_binary=divide(encode_message)
            liste_binary.append(to_8bits_binary(lastremain))
            # print(liste_binary)

            writebytes(o,tobytes_array(liste_binary))
    @staticmethod
    def decompress(inputfile,outputfile):
        binary=read_binary_bytes(inputfile)
        codes=''
        alava=base_vers_decimal(binary[-1],2)
        binary[-2]=binary[-2][-alava:]
        codes=''.join(binary[:-1])
        
        with open(inputfile+'.map','r') as map,open(outputfile,'w') as output:
            map_content=map.read()
        
            lines=map_content.split('\n')
            #alava total
            alava=int(lines[0])

            manytime={}
            element=lines[1:len(lines)-1]
            for i in element:
                temp=i.split(':')
                manytime[chr(int(temp[0]))]=int(temp[1],2)/alava
            a=Encoder()
            output.write(a.decode(codes,dictionary=manytime))
        

