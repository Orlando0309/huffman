def divide(str_binary):
    chunks = [str_binary[i:i+8].zfill(8) for i in range(0, len(str_binary), 8)]
    return chunks


def writebytes(inputs,arrays_byte):
    with open(inputs, 'wb') as f:
        f.write(b''.join(arrays_byte))

def to_bytes(codes):
    num_bytes = (len(codes) + 7) // 8
    # print(str(num_bytes)+'=>'+codes,int(codes,2))
    return int(codes, 2).to_bytes(num_bytes, byteorder='big')

def tobytes_array(liste_binary):
    return [ to_bytes(i) for i in liste_binary]

#read the file with byte values and return the bits value
def read_binary_bytes(file_bin):
    binary_bytes = []
    with open(file_bin, 'rb') as f:
        byte = f.read(1)
        while byte:
            binary_bytes.append(format(ord(byte), '08b'))
            byte = f.read(1)
    return binary_bytes

def to_8bits_binary(number):
    binary_string = format(number, 'b')
    return binary_string.zfill(8)
